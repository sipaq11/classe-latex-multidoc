La classe LaTeX multidoc a été faite pour aider à développer du matériel de cours. 

Les principaux objectifs de la classe sont :

- Faciliter l'organisation et la mise en page des documents pour des débutants avec LaTeX.

- Permettre d'inscrire à un seul endroit le contenu (théorèmes, exemples et autres éléments) et de préciser, à l'aide d'une commande, les documents concernés.

- Fournir des commandes reconnues dans tous les types de documents.

- Donner un accès plus facile à certaines fonctionnalités (exemple : infobulle).

- Faciliter l'uniformité (texte, numérotation, ...).


Cinq documents pédagogiques peuvent être créés avec la classe :

- Un manuel numérique optimisé pour la lecture sur un ordinateur par l'application gratuite Acrobat Reader.

- Un manuel numérique optimisé pour la lecture sur un appareil mobile.

- Des diapositives (beamer) pour les présentations en classe.

- Des notes de cours à compléter par les élèves. Le contenu des notes de cours est le même que celui des diapositives, mais adapté à une impression sur du papier de format lettre.

- Un fichier qui contient uniquement les séries d'exercices du manuel et leurs réponses.

La classe permet d'incorporer des infobulles. Celles-ci sont générées préalablement dans un fichier distinct avant d'être utilisées dans les autres.

Une documentation complète des fonctionnalités et de l'utilisation de la classe est disponible.
